﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.Infrastructure
{
    public static class API
    {
        public static class Children
        {
            public static string GetAll(string baseUri) => $"{baseUri}/children";
            public static string GetChild(string baseUri, string id) => $"{baseUri}/children/{id}";

            internal static object GetChild(object baseUri, string id)
            {
                throw new NotImplementedException();
            }
        }

        public static class Report
        {
            public static string GetChildrenReport(string baseUri) => $"{baseUri}/children";
            public static string GetChildReport(string baseUri, string id) => $"{baseUri}/children/{id}";
        }
    }
}
