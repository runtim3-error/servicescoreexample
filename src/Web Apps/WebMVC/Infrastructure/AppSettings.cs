﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.Infrastructure
{
    public class AppSettings
    {
        //public Connectionstrings ConnectionStrings { get; set; }
        public string IdentityUrl { get; set; }
        public string ChildrenUrl { get; set; }
        public string ReportUrl { get; set; }
    }
}
