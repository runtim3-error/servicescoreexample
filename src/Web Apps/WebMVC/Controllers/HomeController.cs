﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebMVC.Models;
using WebMVC.Services;

namespace WebMVC.Controllers
{
    public class HomeController : Controller
    {
        private IChildrenService _childrenService;

        public HomeController(IChildrenService childrenService)
        {
            _childrenService = childrenService;
        }

        public IActionResult Index()
        {
            return View(new ViewModels.Home.IndexViewModel());
        }

        [Authorize]
        public async Task<IActionResult> Children()
        {
            ViewData["Message"] = "Your application description page.";

            try
            {
                var children = await _childrenService.GetChildren();

                return View(children);
            }
            catch (Polly.CircuitBreaker.BrokenCircuitException ex)
            {
                TempData["ErrorMessage"] = "К сожалению не удалось получить отчет. Пожалуйста, повторите попытку позже.";
                return Redirect(nameof(Error));
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();


            return View(new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                ErrorMessage = TempData["ErrorMessage"]?.ToString() ?? exceptionHandlerPathFeature?.Error?.Message
            });
        }
    }
}
