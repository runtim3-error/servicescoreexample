﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebMVC.Infrastructure;

namespace WebMVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IOptions<AppSettings> _settings;

        public AccountController(IOptions<AppSettings> settings)
        {
            _settings = settings;
        }

        public IActionResult Index()
        {
            return RedirectToAction(nameof(Profile));
        }

        public IActionResult Profile()
        {
            return Redirect($"{_settings.Value.IdentityUrl}/Identity/Account/Manage");
        }

        public IActionResult Login()
        {
            //Controller is Authorize, so it automatically will call IdentityServer login
            return Redirect("~/");
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            return SignOut("Cookies", "oidc");
        }
    }
}