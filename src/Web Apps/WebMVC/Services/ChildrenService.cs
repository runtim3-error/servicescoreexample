﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebMVC.ViewModels;
using WebMVC.Infrastructure;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;

namespace WebMVC.Services
{
    public class ChildrenService : IChildrenService
    {
        private HttpClient _httpClient;
        private string _baseUri;

        public ChildrenService(HttpClient httpClient, IOptions<AppSettings> settings, IHttpContextAccessor httpContextAccessor)
        {
            _httpClient = httpClient;
            _baseUri = $"{settings.Value.ChildrenUrl}/api";
        }

        public Task AddChild(Child child)
        {
            throw new NotImplementedException();
        }

        public async Task<Child> GetChild(string id)
        {
            var uri = API.Children.GetChild(_baseUri, id);

            var response = await _httpClient.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var responseString = await response.Content.ReadAsStringAsync();

            return string.IsNullOrEmpty(responseString)
                ? null
                : JsonConvert.DeserializeObject<Child>(responseString);
        }

        public async Task<IEnumerable<Child>> GetChildren()
        {
            var uri = API.Children.GetAll(_baseUri);

            var responseString = await _httpClient.GetStringAsync(uri);

            return string.IsNullOrEmpty(responseString) 
                ? Enumerable.Empty<Child>()
                : JsonConvert.DeserializeObject<IEnumerable<Child>>(responseString);
        }
    }
}
