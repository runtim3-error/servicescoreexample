﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMVC.ViewModels;

namespace WebMVC.Services
{
    public interface IChildrenService
    {
        Task<IEnumerable<Child>> GetChildren();
        Task<Child> GetChild(string id);
        Task AddChild(Child child);
    }
}
