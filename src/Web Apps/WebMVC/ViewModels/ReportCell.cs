﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WebMVC.ViewModels
{
    public class ReportCell
    {
        [Display(Name = "Значение")]
        public string Value { get; set; }

        public int ColumnSpan { get; set; }
    }
}