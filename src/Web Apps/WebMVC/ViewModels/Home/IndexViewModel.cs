﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.ViewModels.Home
{
    public class IndexViewModel
    {
        public IEnumerable<string> BannersURL { get; set; }
    }
}
