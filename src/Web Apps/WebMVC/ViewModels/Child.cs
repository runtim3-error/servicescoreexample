﻿namespace WebMVC.ViewModels
{
    public class Child
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}