﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.ViewModels
{
    public class ReportTable
    {
        public IEnumerable<ReportRow> ReportRows { get; set; }

        public int ColumnsCount { get; set; }
    }
}
