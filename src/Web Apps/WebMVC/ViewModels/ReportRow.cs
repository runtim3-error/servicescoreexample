﻿using System.Collections.Generic;

namespace WebMVC.ViewModels
{
    public class ReportRow
    {
        public IEnumerable<ReportCell> ReportColumns { get; set; }
    }
}