﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Children.API.Models;
using Microsoft.AspNetCore.Authorization;
using IdentityModel;

namespace Children.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildrenController : ControllerBase
    {
        // GET api/children
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Child>), (int)HttpStatusCode.OK)]
        public ActionResult<IEnumerable<Child>> Get()
        {
            var child = new Child()
            {
                Id = User.Claims.FirstOrDefault(x=> x.Type == JwtClaimTypes.Subject)?.Value,
                Name = User.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email)?.Value,
            };

            return new[] { child };
        }
    }
}
