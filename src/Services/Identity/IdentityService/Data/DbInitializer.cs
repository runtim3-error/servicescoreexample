﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Data
{
    public class DbInitializer
    {
        private ApplicationDbContext _applicationDbContext;

        public DbInitializer(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Init()
        {
            _applicationDbContext.Database.Migrate();
            //dbContext.Database.EnsureCreated();

            //var pendingMigrations = dbContext.Database.GetAppliedMigrations().ToList();
            //if (pendingMigrations.Any())
            //{
            //    var migrator = dbContext.Database.GetService<IMigrator>();
            //    foreach (var targetMigration in pendingMigrations)
            //    {
            //        migrator.Migrate(targetMigration); 
            //    }
            //}
        }
    }
}
