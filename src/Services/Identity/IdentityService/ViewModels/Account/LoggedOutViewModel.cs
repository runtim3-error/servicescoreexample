﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


namespace IdentityService.ViewModels.Account
{
    public class LoggedOutViewModel
    {
        private bool _redirectTimeout = false;

        public string PostLogoutRedirectUri { get; set; }
        public string ClientName { get; set; }
        public string SignOutIframeUrl { get; set; }

        public bool AutomaticRedirectAfterSignOut { get; set; } = false;
        public bool AutomaticRedirectAfterSignOutTimeout
        {
            get
            {
                return _redirectTimeout;
            }
            set
            {
                _redirectTimeout = value;
                if (value)
                {
                    AutomaticRedirectAfterSignOut = true; 
                }
            }
        }

        public string LogoutId { get; set; }
        public bool TriggerExternalSignout => ExternalAuthenticationScheme != null;
        public string ExternalAuthenticationScheme { get; set; }
    }
}