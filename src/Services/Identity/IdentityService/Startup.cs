﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityService.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;
using System.Security.Claims;
using IdentityService.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace IdentityService
{
    public class Startup
    {
        private ILogger<Startup> _logger;

        public Startup(IConfiguration configuration, IHostingEnvironment environment, ILogger<Startup> logger)
        {
            Configuration = configuration;
            HostingEnvironment = environment;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddDbContextCheck<ApplicationDbContext>();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            var connectionString = Configuration["DefaultConnection"] ?? Configuration.GetConnectionString("DefaultConnection");

            _logger.LogWarning("Connection string for database={0}", connectionString);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddScoped<DbInitializer>();
            services.AddScoped<IEmailSender, Services.EmailSender>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<IISOptions>(iis =>
            {
                iis.AuthenticationDisplayName = "Windows";
                iis.AutomaticAuthentication = false;
            });

            services.AddCustomIdentity(Configuration, out var builder);

            if (HostingEnvironment.IsDevelopment())
            {
                builder.AddDeveloperSigningCredential();
            }
            else
            {
                throw new Exception("need to configure key material");
            }

            services.AddCustomAuthentication(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    policyBuilder => policyBuilder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseHealthChecks("/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseIdentityServer();

            app.UseMvcWithDefaultRoute();
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    // register your IdentityServer with Google at https://console.developers.google.com
                    // enable the Google+ API
                    // set the redirect URI to http://localhost:5000/signin-google
                    options.ClientId = "121538499654-the2fdlffdali312unp9fpjpqbneldgo.apps.googleusercontent.com";
                    options.ClientSecret = "6T9Uy3ekpdO45Q1QqoEsZbZt";
                })
                //.AddOAuth("Bitrix24", options =>
                //{
                //    options.ClientId = "IdentityService";
                //    options.ClientSecret = "bitrix_secret";
                //    options.Scope.Add("r_basicprofile");
                //    options.Scope.Add("r_emailaddress");
                //    options.CallbackPath = "/signin-bitrix";
                //    options.AuthorizationEndpoint = "https://portal.bitrix24.com/oauth/authorize";
                //    options.TokenEndpoint = "https://oauth.bitrix.info/oauth/token/";
                //    options.Events = new Microsoft.AspNetCore.Authentication.OAuth.OAuthEvents
                //    {
                //        OnCreatingTicket = async context =>
                //        {
                //            var request = new System.Net.Http.HttpRequestMessage(System.Net.Http.HttpMethod.Get, context.Options.UserInformationEndpoint);
                //            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", context.AccessToken);
                //            request.Headers.Add("x-li-format", "json");

                //            var response = await context.Backchannel.SendAsync(request, context.HttpContext.RequestAborted);
                //            response.EnsureSuccessStatusCode();
                //            var user = Newtonsoft.Json.Linq.JObject.Parse(await response.Content.ReadAsStringAsync());

                //            var userId = user.Value<string>("id");
                //            if (!string.IsNullOrEmpty(userId))
                //            {
                //                context.Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId, ClaimValueTypes.String, context.Options.ClaimsIssuer));
                //            }

                //            var formattedName = user.Value<string>("formattedName");
                //            if (!string.IsNullOrEmpty(formattedName))
                //            {
                //                context.Identity.AddClaim(new Claim(ClaimTypes.Name, formattedName, ClaimValueTypes.String, context.Options.ClaimsIssuer));
                //            }

                //            var email = user.Value<string>("emailAddress");
                //            if (!string.IsNullOrEmpty(email))
                //            {
                //                context.Identity.AddClaim(new Claim(ClaimTypes.Email, email, ClaimValueTypes.String,
                //                    context.Options.ClaimsIssuer));
                //            }
                //            var pictureUrl = user.Value<string>("pictureUrl");
                //            if (!string.IsNullOrEmpty(pictureUrl))
                //            {
                //                context.Identity.AddClaim(new Claim("profile-picture", pictureUrl, ClaimValueTypes.String,
                //                    context.Options.ClaimsIssuer));
                //            }
                //        }
                //    };
                //})
                .AddCookie(options =>
                {
                    options.LoginPath = "~/Account/Login";
                    options.LogoutPath = "~/Account/Logout";
                })
                ;
            return services;
        }

        public static IServiceCollection AddCustomIdentity(this IServiceCollection services, IConfiguration configuration, out IIdentityServerBuilder builder)
        {

            services.AddIdentity<ApplicationUser, IdentityRole>()
                //.AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            builder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                    //options.IssuerUri = configuration["IDENTITY_ISSUER"];
                })
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApis())
                .AddInMemoryClients(Config.GetClients(configuration))
                .AddAspNetIdentity<ApplicationUser>()
                .AddExtensionGrantValidator<Helpers.DelegationGrantValidator>();

            return services;
        }
    }

}
