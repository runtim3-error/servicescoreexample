﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService
{
    public static class Config
    {
        const string ChildrenScope = "children_api";

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            yield return new ApiResource(ChildrenScope, "Children API")
            {
                UserClaims =
                {
                    JwtClaimTypes.Name,
                    JwtClaimTypes.Email,
                    JwtClaimTypes.Role,
                }
            };
        }

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            var adresses = new[]
            {
                $"{configuration["MvcUrl"]}",
                "http://localhost:5000",
                "https://localhost:4999",
                "http://localhost:4998",
                "http://localhost:6000",
                "https://localhost:6999",
                "http://localhost:6998"
            };

            yield return new Client
            {
                ClientId = "mvc",
                ClientName = "MVC Client",
                AllowedGrantTypes = GrantTypes.Hybrid,

                ClientSecrets =
                {
                    new Secret("secret".Sha256())
                },

                RedirectUris = adresses.Select(x=> $"{x}/signin-oidc").ToArray(),
                PostLogoutRedirectUris = adresses.Select(x => $"{x}/signout-callback-oidc").ToArray(),

                AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        ChildrenScope
                    },

                AllowOfflineAccess = false,
                RequireConsent = false,
            };
        }
    }
}
