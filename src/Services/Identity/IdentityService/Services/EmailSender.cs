﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityService.Services
{
    public class EmailSender : IEmailSender
    {
        private ILogger<EmailSender> _logger;

        public EmailSender(ILogger<EmailSender> logger)
        {
            _logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            _logger.LogDebug("Email={0}; Subject={1}; htmlMessage={3}", email, subject, htmlMessage);
            //SEND EMAIL
            await Task.Delay(500);
        }
    }
}
