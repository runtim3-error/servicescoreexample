﻿/// <binding AfterBuild='copy_awesome_fonts, copy_popper_map, copy_bootstrap_fonts' />
var gulp = require('gulp');

var folders = {
    root: "./wwwroot/",
    node: "./node_modules/"
};

gulp.task('copy_awesome_fonts', function () {
    return gulp.src([folders.node + '/font-awesome-5-css/webfonts/*']).pipe(gulp.dest(folders.root + 'webfonts/'));
})

gulp.task('copy_popper_map', function () {
    return gulp.src(folders.node + 'popper.js/dist/umd/popper.min.js.map').pipe(gulp.dest(folders.root + 'bundle/'));
})

gulp.task('copy_bootstrap_fonts', function () {
    return gulp.src([folders.node + '/bootstrap/dist/fonts/*']).pipe(gulp.dest(folders.root + 'fonts/'));
})

