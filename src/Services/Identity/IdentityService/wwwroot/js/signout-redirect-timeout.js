﻿
var countdown = 5000;

var setSeconds = function (ms) {
    document.getElementById("countdown").innerHTML = Math.floor(ms / 1000) + "s ";
}

$(document).ready(function () {
    setSeconds(countdown);
});

var curentValue = null;
var x = setInterval(function () {

    if (curentValue == null) {
        curentValue = countdown;
    }

    curentValue = curentValue - 1000;

    // If the count down is finished, write some text 
    if (curentValue < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Перенаправление";
        return;
    }
    // Display the result in the element with id="demo"
    setSeconds(curentValue);
}, 1000);

window.setTimeout(function () {
    var a = document.querySelector("a.PostLogoutRedirectUri");
    if (a) {
        window.location = a.href;
    }
}, countdown);
