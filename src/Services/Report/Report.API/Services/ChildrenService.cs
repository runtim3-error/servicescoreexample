﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Report.API.Infrastructure;
using Report.API.Models;

namespace Report.API.Services
{
    public class ChildrenService : IChildrenService
    {
        private readonly HttpClient _apiClient;
        private string _childrenByPassUrl;
        private IOptions<AppSettings> _settings;
        private IHttpContextAccessor _httpContextAccessor;

        public ChildrenService(HttpClient httpClient, IOptions<AppSettings> settings, IHttpContextAccessor httpContextAccessor)
        {
            _apiClient = httpClient;
            _childrenByPassUrl = $"{settings.Value.ChildrenUrl}/api";
            _settings = settings;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Child> GetChild(string id)
        {
            var uri = Infrastructure.API.Children.GetChild(_childrenByPassUrl, id);

            await UpdateToken();

            var responseString = await _apiClient.GetStringAsync(uri);

            return string.IsNullOrEmpty(responseString)
                ? null
                : JsonConvert.DeserializeObject<Child>(responseString);
        }

        public async Task<IEnumerable<Child>> GetChildren()
        {
            var uri = Infrastructure.API.Children.GetAll(_childrenByPassUrl);

            await UpdateToken();

            var responseString = await _apiClient.GetStringAsync(uri);

            return string.IsNullOrEmpty(responseString) 
                ? Enumerable.Empty<Child>()
                : JsonConvert.DeserializeObject<IEnumerable<Child>>(responseString);
        }

        private async Task UpdateToken()
        {
            var tokenResponse = await DelegateAsync();
            if (tokenResponse.IsError)
            {
                return;
            }
            _apiClient.SetBearerToken(tokenResponse.AccessToken);
        }

        private async Task<TokenResponse> DelegateAsync()
        {
            var token = await _httpContextAccessor.HttpContext.GetTokenAsync("access_token");
            // create token client
            var disco = await _apiClient.GetDiscoveryDocumentAsync(_settings.Value.IdentityUrl);

            return await _apiClient.RequestTokenAsync(new TokenRequest()
            {
                ClientId = "report.api",
                ClientSecret = "secret",
                Address = disco.TokenEndpoint,
                GrantType = "delegation",
                Parameters =
                {
                    {"scope","children_api" },
                    {"token", token }
                }
            });
        }
    }
}
