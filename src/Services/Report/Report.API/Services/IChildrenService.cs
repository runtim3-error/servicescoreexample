﻿using Report.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Report.API.Services
{
    public interface IChildrenService
    {
        Task<IEnumerable<Child>> GetChildren();
        Task<Child> GetChild(string id);
    }
}
