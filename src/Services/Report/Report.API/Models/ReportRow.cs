﻿using System;
using System.Collections.Generic;

namespace Report.API.Models
{
    public class ReportRow
    {
        public ReportRow(params string[] values) : this()
        {
            foreach (var item in values)
            {
                ReportColumns.Add(new ReportCell(item));
            }
        }

        public ReportRow()
        {
            ReportColumns = new List<ReportCell>();
        }

        //public IEnumerable<ReportCell> Columns => ReportColumns;

        public IList<ReportCell> ReportColumns { get; }
    }
}