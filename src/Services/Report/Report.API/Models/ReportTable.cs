﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Report.API.Models
{
    public class ReportTable
    {
        public ReportTable()
        {
            ReportRows = new List<ReportRow>();
            ReportHeaders = new List<ReportTableHeaders>();
        }

        public ReportTable(params ReportRow[] rows) : this()
        {
            foreach (var row in rows)
            {
                ReportRows.Add(row);
            }
        }

        public IList<ReportRow> ReportRows { get; }

        public IList<ReportTableHeaders> ReportHeaders { get; }

        public int ColumnsCount => ReportRows.Any() ? ReportRows.Max(x => x.ReportColumns.Any() ? x.ReportColumns.Sum(z => z.ColumnSpan) : x.ReportColumns.Count) : 0;

        internal ReportTable NormalizedTable
        {
            get
            {
                if (!IsNormalized)
                {
                    NormalizeColumnSpan();
                }
                return this;
            }
        }
        
        private bool IsNormalized
        {
            get
            {
                var count = ColumnsCount;
                return ReportRows.All(x => !x.ReportColumns.Any() || x.ReportColumns.Sum(z => z.ColumnSpan) == count);
            }
        }

        private void NormalizeColumnSpan()
        {
            foreach (var row in ReportRows)
            {
                var divisionSpan = Division(ColumnsCount, row.ReportColumns.Count).Reverse().ToArray();

                if (divisionSpan.Length != row.ReportColumns.Count)
                {
                    continue;
                }

                foreach (var cell in row.ReportColumns)
                {
                    cell.ColumnSpan = divisionSpan[row.ReportColumns.IndexOf(cell)];
                }
            }
        }

        private IEnumerable<int> Division(int value, int partCount)
        {
            var result = new List<int>();
            int cellSpan = value;
            while (value > 0 && partCount > 0)
            {
                if (cellSpan % 2 == 0)
                    cellSpan = (int)Math.Floor((double)(value / partCount));
                else
                    cellSpan = (int)Math.Ceiling((double)(value / partCount));
                value -= cellSpan;
                partCount--;
                result.Add(cellSpan);
            }
            return result;
        }
    }
}
