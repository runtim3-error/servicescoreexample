﻿using System;

namespace Report.API.Models
{
    public class Child
    {
        //public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Date { get; set; }
    }
}