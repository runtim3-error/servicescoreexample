﻿namespace Report.API.Models
{
    public class ReportCell
    {
        public ReportCell()
        {
            ColumnSpan = 1;
        }

        public ReportCell(string value) : this()
        {
            Value = value;
        }

        public ReportCell(string value, int columnSpan) : this(value)
        {
            ColumnSpan = columnSpan;
        }

        public string Value { get; set; }
        public int ColumnSpan { get; set; }
    }
}