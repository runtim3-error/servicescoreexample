﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Report.API.Infrastructure
{
    public class AppSettings
    {
        public string IdentityUrl { get; set; }
        public string ChildrenUrl { get; set; }
    }
}
