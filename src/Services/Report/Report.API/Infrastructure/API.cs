﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Report.API.Infrastructure
{
    public static class API
    {
        public static class Children
        {
            public static string GetAll(string baseUri) => $"{baseUri}/children";
            public static string GetChild(string baseUri, string id) => $"{baseUri}/children/{id}";
        }
    }
}
