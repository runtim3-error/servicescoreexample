﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HealthChecks.UI.Client;
using IdentityModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Extensions.Http;
using Report.API.Infrastructure;

namespace Report.API
{
    public class Startup
    {
        private ILogger _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //DI
            services.AddScoped<Services.IChildrenService, Services.ChildrenService>();
            services.AddHttpClient<Services.IChildrenService, Services.ChildrenService>()
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (reqMsg, cert, chain, sslPolicyErrors) => true,
                })
                .SetHandlerLifetime(TimeSpan.FromMinutes(3))
                .AddPolicyHandler(GetRetryPolicy(_logger))
                .AddPolicyHandler(GetCircuitBreakerPolicy())
            ;

            //HEALTH
            services.AddHealthChecks()
                .AddCheck("self", (x) => Microsoft.Extensions.Diagnostics.HealthChecks.HealthCheckResult.Healthy("Проверок не выполнялось"))
                .AddUrlGroup(new Uri($"{Configuration["ChildrenUrl"]}/hc"), name: "Children API service")
                .AddUrlGroup(new Uri($"{Configuration["IdentityUrl"]}/hc"), name: "Identity service");

            //for DI IHttpContextAccessor in services
            services.AddHttpContextAccessor();
            //prevent changing name to "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name" and another claim types
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            //AUTH
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = Configuration["IdentityAuthority"];
                options.Audience = "report_api";
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    NameClaimType = JwtClaimTypes.Name,
                    RoleClaimType = JwtClaimTypes.Role,
                };
            });

            //MVC
            services
                .AddMvc(options =>
                {
                    options.RespectBrowserAcceptHeader = true;
                })
                .AddXmlDataContractSerializerFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddOptions();
            services.Configure<AppSettings>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHealthChecks("/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            //app.UseHttpsRedirection();
            app.UseMvc();
        }

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy(ILogger logger)
        {
            Random jitterer = new Random();
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => 
                {
                    if (msg.StatusCode == System.Net.HttpStatusCode.TemporaryRedirect)
                    {
                        var content = msg.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        logger.LogWarning($"307 was returned. Content = {content}; Location={msg.Headers.Location}");
                    }
                    return msg.StatusCode == System.Net.HttpStatusCode.NotFound;
                })
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)) 
                                                    + TimeSpan.FromMilliseconds(jitterer.Next(0, 100)));
        }

        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(2, TimeSpan.FromSeconds(30));
        }
    }
}
