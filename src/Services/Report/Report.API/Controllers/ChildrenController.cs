﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Report.API.Services;
using Report.API.Models;
using Microsoft.AspNetCore.Authorization;
using IdentityModel;

namespace Report.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChildrenController : ControllerBase
    {
        private IChildrenService _childrenService;

        public ChildrenController(IChildrenService childrenService) => _childrenService = childrenService;

        [HttpGet]
        public async Task<ActionResult<ReportTable>> Get()
        {
            var table = new ReportTable();
            IEnumerable<Child> children;
            try
            {
                children = await _childrenService.GetChildren();
            }
            catch
            {
                return BadRequest("Can't get children from service");
            }
            var gen = new Random((int)DateTime.UtcNow.Ticks);

            var start = new DateTime(1995, 1, 1);
            var end = start.AddYears(5);
            int range = (end - start).Days;

            foreach (var child in children)
            {
                table.ReportRows.Add(new ReportRow(child.Name, child.Surname, start.AddDays(gen.Next(range)).ToLongDateString(), child.Date.ToShortDateString()));
            }
            table.ReportRows.Add(new ReportRow("", "Итого", children.Count().ToString()));
            return Ok(table.NormalizedTable);
        }

        [HttpGet]
        [Route("{id}")]
        [Authorize]
        public ActionResult<ReportTable> Get(string id)
        {
            var claimSub = User.Claims.FirstOrDefault(x => x.Type == System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
            var claimName = User.Claims.FirstOrDefault(x => x.Type == System.Security.Claims.ClaimTypes.Name)?.Value;

            var table = new ReportTable(User.Claims.Select(x => new ReportRow("Тип:", x.Type, "Значение:", x.Value)).ToArray());

            table.ReportRows.Insert(0, new ReportRow($"Claims of user {id}"));

            table.ReportRows.Add(new ReportRow("Sub (Security):", claimSub, "Claim name (Security):", claimName));
            table.ReportRows.Add(new ReportRow("Sub (JWT):", User.Claims.FirstOrDefault(x=> x.Type == JwtClaimTypes.Subject)?.Value, "Claim name (JWT)", User.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name)?.Value));

            table.ReportRows.Add(new ReportRow("Identity:", User.Identity.Name ?? "Identity name is null", User.Identity.IsAuthenticated ? "Authenticated" : "Not authenticated", User.Identity.AuthenticationType ?? "Identity authentication type is null"));

            return Ok(table.NormalizedTable);
        }
    }
}
